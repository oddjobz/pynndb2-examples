"""
 _           _ _         _                            _
| |__  _   _| | | __    (_)_ __ ___  _ __   ___  _ __| |_
| '_ \| | | | | |/ /____| | '_ ` _ \| '_ \ / _ \| '__| __|
| |_) | |_| | |   <_____| | | | | | | |_) | (_) | |  | |_
|_.__/ \__,_|_|_|\_\    |_|_| |_| |_| .__/ \___/|_|   \__|
                                    |_|

This example is designed to demonstrate how large volumes of data can be imported (quickly)
 from CSV style files. The initial entry point is via 'main' which instantiates MPQServer, which
 will read entries from the file passed via argv[] and distribute them between worker processes.
 Each worker process is an instance of "Client" and is a self-contained black box, except for the
 "process_message" method which the programmer needs to override.

"""
from __future__ import annotations
from mpq_server import MPQServer
from mpq_client import MPQClient
from ujson import loads, dumps
from bson import ObjectId
from sys import argv


class Client(MPQClient):
    """
    This class is based on MPQClient (which does all the work), our only responsibility
    it to implement the custom logic that will parse the data for the format of source
    file we want to process.
    """

    def process_message(self, message: bytes) -> None:
        """
        Process the incoming message in a way that we see fit generating two components which
        then neeed to be appended as a tuple (oid, data) to self._data. This is 'extremely' performance
        sensitive for large inputs, so some python decorum goes out the window, but hopefully
        it's still readable.

        message - the incoming message to process
        """
        message = message.decode()
        brace = message.find('{"dataset')
        json = loads(message[brace:])
        text = message[:brace].split()
        json['sense'] = text[-1]
        self._data.append((dumps(json).encode(), str(ObjectId()).encode()))


class Main:
    """
    This class is used to wrap the main() entry point for this example and is called by default
    when the module is loaded, the "run" method is subsequently called with the first parameter
    passed from the command line.
    """
    DATABASE = '.database'
    TABLE = 'data'
    WORKERS = 1

    def run(self, file_name: str) -> None:
        """
        Just a wrapper for our main point of entry.

        file_name - the name of the file we want to import
        """
        with MPQServer(
                Client,
                database_path=self.DATABASE,
                table_name=self.TABLE,
                workers=self.WORKERS) as server:
            server.process_file(file_name=file_name)


if __name__ == '__main__':
    Main().run(argv[1])

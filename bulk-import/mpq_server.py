#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
from __future__ import annotations
from mpq_client import MPQClient
from multiprocessing import Process
from tqdm import tqdm
import os
import socket
import zmq


class MPQServer:
    """
    Implementation of the server side of the custom bulk importer. This class implements the
    reading of the source file and the distribution of the resulting data to multiple worker
    processes implemented via the MPQClient class. The TQDM library is used to display a
    progress bar, you will need to tweak this manually if you want a silent background import.

    o LOCALHOST    is the location we will be broadcasting our data to (via our ZMQ pipeline)
    """

    LOCALHOST = 'tcp://127.0.0.1'

    def __init__(self, queue_handler: MPQClient, database_path: str, table_name: str, workers=1) -> None:
        """
        Instantiate an instance of the server process, typically you will only want one of these
        at a time.

        queue_handler - the class we will use to generate our fan-out clients
        database_path - the path to our database file
        table_name - the name of the table to write our data to
        workers - the number of worker processes to start
        """
        self._handlers = []
        self._queue_handler = queue_handler
        self._database_path = database_path
        self._table_name = table_name
        self._workers = workers

    def __enter__(self) -> None:
        """
        Context manager for this instance, we use the class inconjunction with "with" to ensure that
        processes are created and terminated cleanly.
        """
        port = self.find_free_port()
        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.PUSH)
        self._socket.bind(f'{self.LOCALHOST}:{port}')
        for worker in range(self._workers):
            client = self._queue_handler(self._database_path, self._table_name, port)
            handler = Process(target=client.run)
            handler.start()
            self._handlers.append(handler)
        return self

    def __exit__(self, *args):
        """
        Ensure we joint all the terminated processes on exit
        """
        for handler in self._handlers:
            handler.join()

    def process_file(self, file_name: str) -> None:
        """
        This is the main routine we're going to call to process the incoming file. There are a few things
        to note in terms of performance and efficiency;

        o We are opening the file in binary mode, this ensures our size calculations are correct for tqdm
        o Binary mode also cuts out any unwanted internal variable conversions
        o We're using 'send' to transfer each line in native form, again to cut down on unwanted conversions
        o TQDM mode is working in 'size' mode, which gives us throughput in M/sec

        file_name - the name of the incoming file to process
        """
        with open(file_name, 'rb') as io:
            io.seek(0, os.SEEK_END)
            size = io.tell()
            io.seek(0, os.SEEK_SET)
            try:
                with tqdm(
                        total=size,
                        unit_scale=True,
                        unit='',
                        unit_divisor=1024) as progress_bar:
                    while True:
                        line = io.readline()
                        if not line:
                            break
                        self._socket.send(line)
                        progress_bar.update(len(line))
            except KeyboardInterrupt:
                print("Cancelled by Ctrl+C")

    @staticmethod
    def find_free_port():
        """
        Find an available listener port that we can use to communicate between the client and
        server processes. This is only for internal use.
        """
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind(('', 0))
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            return s.getsockname()[1]

#####################################################################################
#
#  Copyright (c) 2020 - Mad Penguin Consulting Ltd
#
#####################################################################################
from __future__ import annotations
from pynndb import Manager
from signal import signal, SIGTERM
from setproctitle import setproctitle
import zmq


class MPQClient:
    """
    Base class for an MPQClient instance, this is the fan-out process that will
    reveice messages from the import routine.

    o LOCALHOST        constant pointing to the address of the incoming server
    o TIMEOUT          the idle time we wait for no data before exiting
    o MAP_SIZE         defaults to 20G, override at class level as necessary
    o WRITE_THRESHOLD  number of writes to batch per DB transaction
    """
    LOCALHOST = 'tcp://127.0.0.1'
    TIMEOUT = 1000
    WRITE_THRESHOLD = 5000
    MAP_SIZE = 21474836480
    EOF = b'\xff'

    def __init__(self, database_path: str, table_name: str, port: int) -> None:
        """
        This instantiates a fan-out client instance, it should only every be called from
        the server instance and is used once per worker process.

        database_path - the path to our database file
        table_name - the name of the table to write data into
        port - the port number of our server instance
        """
        self._database_path = database_path
        self._table_name = table_name
        self._port = port
        self._strike = False
        self._data = []

    def handle_sigterm(self, *args):
        """
        Handle SIGTERM as if it were a control+c

        args - not used
        """
        raise KeyboardInterrupt

    def flush(self):
        """
        Flush the current batch to the database, we're expecting the batch to exist in self._data
        as an array of tuples where each tuple consists of a key and data item. This is obviously
        bypassing all the database internals and will not provide any indexing or compression, it
        will however provide a very fast / efficient mechanism for getting data "into" the database.
        """
        with self._table.write_transaction as txn:
            while len(self._data):
                doc, oid = self._data.pop(0)
                txn.put(oid, doc, db=self._table._db)

    def run(self):
        """
        The main routine called in it's own process by a server instance. It expects to be receiving
        data immediately and if it goes for more than a second without receiving data then it assumes
        no more will be coming and terminates.
        """
        setproctitle(f'--Import process--')
        signal(SIGTERM, self.handle_sigterm)
        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.PULL)
        self._socket.connect(f'{self.LOCALHOST}:{self._port}')
        self._socket.setsockopt(zmq.RCVTIMEO, self.TIMEOUT)
        options = {
            'sync': False,
            'metasync': False,
            'map_size': self.MAP_SIZE
        }
        self._db = Manager().database(self._database_path, self._database_path, options)
        self._table = self._db.table(self._table_name)
        try:
            while True:
                message = self.get()
                if message == self.EOF:
                    break
                self.process_message(message)
                if len(self._data) > self.WRITE_THRESHOLD:
                    self.flush()
            self.flush()
            self._db.sync(True)
        except KeyboardInterrupt:
            return

    def process_message(self, message: bytes) -> None:
        """
        This is the routine the coder needs to override, the incoming line will be a series of bytes,
        user-level logic needs to parse this line and store valid data in self._data. Each result needs
        to be a tuple of (key, data), where both variables are "bytes", and the data component is
        a binary encoded JSON blob, for example; dumps({'name': 'fred'}).encode(). You should use
        bson/ObjectId to generate your oid.

        message - a single line from the the incoming file
        """
        raise NotImplemented

    def get(self) -> bytes:
        """
        Get the next message from the server returning eithe the received messages as a string of bytes
        if there was a message, or self.EOF if the receive timed out and we are to assume we've finished.
        """
        while True:
            try:
                return self._socket.recv()
            except zmq.error.Again:
                if self._strike:
                    return self.EOF
                self._strike = True

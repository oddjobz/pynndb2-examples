# Example1

For testing purposes I'm using a datafile downloaded from this location;
[https://github.com/commonsense/conceptnet5/wiki/Downloads](https://github.com/commonsense/conceptnet5/wiki/Downloads)

When uncompressed this file is currently a little under 10G in size and it imports in a little over 2 minutes when using 11 cores on my 12 core machine (AMD Ryzen 5/2600) timings will vary greatly depending on the amount of resource
you throw at it, a single core will take more like 7-8 minutes.

10G over two minutes equates to something like 240k new records per second in the DB.
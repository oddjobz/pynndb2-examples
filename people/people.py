"""
                        _
 _ __   ___  ___  _ __ | | ___
| '_ \\ / _ \\/ _ \\| '_ \\| |/ _ \\
| |_) |  __/ (_) | |_) | |  __/
| .__/ \\___|\\___/| .__/|_|\\___|
|_|              |_|

Copyright &copy;2020 - Mad Penguin Consulting Limited
"""
from pynndb import Manager, Doc
from loguru import logger


class People:
    """
    This is a very simple example to demonstrate how to create a database, insert some data,
    then recover it. A sample run looks something line this;
    ```
    2020-05-13 19:23:40.545 | INFO | __main__:open:39 - ** SETUP **
    2020-05-13 19:23:40.546 | INFO | __main__:simple_scan:56 - ** SIMPLE SCAN **
    2020-05-13 19:23:40.546 | INFO | __main__:simple_scan:58 - Tom Jones is 21 years old
    2020-05-13 19:23:40.546 | INFO | __main__:simple_scan:58 - Squizzey is 3000 years old
    2020-05-13 19:23:40.546 | INFO | __main__:simple_scan:58 - Fred Bloggs is 45 years old
    2020-05-13 19:23:40.546 | INFO | __main__:simple_scan:58 - John Doe is 0 years old
    2020-05-13 19:23:40.546 | INFO | __main__:simple_scan:58 - John Smith is 40 years old
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_name:64 - ** SIMPLE SCAN BY NAME **
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_name:66 - Fred Bloggs sorted alphabetically
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_name:66 - John Doe sorted alphabetically
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_name:66 - John Smith sorted alphabetically
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_name:66 - Squizzey sorted alphabetically
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_name:66 - Tom Jones sorted alphabetically
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_age:72 - ** SIMPLE SCAN BY AGE**
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_age:74 - 0 - John Doe in ascending order of age
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_age:74 - 21 - Tom Jones in ascending order of age
    2020-05-13 19:23:40.547 | INFO | __main__:scan_by_age:74 - 40 - John Smith in ascending order of age
    2020-05-13 19:23:40.548 | INFO | __main__:scan_by_age:74 - 45 - Fred Bloggs in ascending order of age
    2020-05-13 19:23:40.548 | INFO | __main__:scan_by_age:74 - 3000 - Squizzey in ascending order of age
    2020-05-13 19:23:40.548 | INFO | __main__:scan_with_filter:78 - ** SIMPLE SCAN WITH A FILTER**
    2020-05-13 19:23:40.548 | INFO | __main__:scan_with_filter:80 - Fred Bloggs is 45 years (filtered age>40)
    2020-05-13 19:23:40.548 | INFO | __main__:scan_with_filter:80 - Squizzey is 3000 years (filtered age>40)
    2020-05-13 19:23:40.548 | INFO | __main__:close:86 - ** TEARDOWN **
    ```
    """

    DATA = [
        {'name': 'Tom Jones', 'age': 21},
        {'name': 'Squizzey', 'age': 3000},
        {'name': 'Fred Bloggs', 'age': 45},
        {'name': 'John Doe', 'age': 0},
        {'name': 'John Smith', 'age': 40},
    ]

    def __init__(self):
        """
        Just initialise instance wide variables
        """
        self._database = None
        self._people = None

    def open(self):
        """
        Open the database, create the table, and index the table by person name and person age
        """
        logger.info('** SETUP **')
        self._database = Manager().database('db', '.people')
        self._people = self._database.table('people')
        self._people.ensure('by_name', '{name}')
        self._people.ensure('by_age', '{age:>3}', duplicates=True)

    def populate(self):
        """
        Populate the people table from the static variable 'DATA' defined in the class definition
        """
        for item in self.DATA:
            self._people.append(Doc(item))

    def simple_scan(self):
        """
        Simple sequential scan through the people table on the primary key
        """
        logger.info('** SIMPLE SCAN **')
        for doc in self._people.find():
            logger.info(f'{doc._name} is {doc._age} years old')

    def scan_by_name(self):
        """
        Scan through the people table using the 'by_name' index to show records in name order
        """
        logger.info('** SIMPLE SCAN BY NAME **')
        for doc in self._people.find('by_name'):
            logger.info(f'{doc._name} sorted alphabetically')

    def scan_by_age(self):
        """
        Scan through the people table using the 'by_age' index to show records in age order
        """
        logger.info('** SIMPLE SCAN BY AGE**')
        for doc in self._people.find('by_age'):
            logger.info(f'{doc._age} - {doc._name} in ascending order of age')

    def scan_with_filter(self):
        """Scan through the people table on the name index filtering records where age > 40"""
        logger.info('** SIMPLE SCAN WITH A FILTER**')
        for doc in self._people.find('by_name', expression=lambda doc: doc['age'] > 40):
            logger.info(f'{doc._name} is {doc._age} years old (filtered age>40)')

    def close(self):
        """
        Drop the people table and close the database
        """
        logger.info('** TEARDOWN **')
        self._database.drop('people')
        self._database.close()

    def main(self):
        """
        main()
        """
        self.open()
        self.populate()
        self.simple_scan()
        self.scan_by_name()
        self.scan_by_age()
        self.scan_with_filter()
        self.close()


if __name__ == '__main__':
    People().main()

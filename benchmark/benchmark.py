"""
 _                     _                          _
| |__   ___ _ __   ___| |__  _ __ ___   __ _ _ __| | __
| '_ \\ / _ \\ '_ \\ / __| '_ \\| '_ ` _ \\ / _` | '__| |/ /
| |_) |  __/ | | | (__| | | | | | | | | (_| | |  |   <
|_.__/ \\___|_| |_|\\___|_| |_|_| |_| |_|\\__,_|_|  |_|\\_\\

Copyright &copy;2020 - Mad Penguin Consulting Limited
"""
from pynndb import Manager, Database, Table, Doc
from loguru import logger
from datetime import datetime
from random import random
from shutil import rmtree


class Benchmark:
    """
    Example benchmarking tool for PyNNDB, runs a number of
    very basic scenario's just to get a feel for the sort
    of performance we're getting on any given machine.
    """

    def persec(self, action: str, start: int, count: int, beg: datetime):
        """
        Simple timing routine to log how long a particular chunk took to write

        action - description of the action being timed
        start - the starting sid / index to use
        count - the number of records to write
        beg - the time the test started
        """
        end = datetime.now()
        rps = int(count / (end - beg).total_seconds())
        logger.info(f'{action}: speed={rps}/sec | {start}:{count}')

    def chunk(self, database: Database, table: Table, start: int, count: int):
        """
        This method is designed to write a chunk of data into the selected datebase -> table
        for subsequent read-speed testing.

        database - the database to write into
        table - the table within the database to write into
        start - a start point for the 'sid' index field
        count - the number of entries to write
        """
        beg = datetime.now()
        for index, session in enumerate(range(start, start + count)):
            with database.write_transaction as txn:
                rnd = random()
                table.append(Doc({
                    'origin': 'linux.co.uk',
                    'sid': start + count - index,
                    'when': datetime.now().timestamp(),
                    'day': int(rnd * 6),
                    'hour': int(rnd * 24)
                }), txn=txn)
        self.persec('Append', start, count, beg)

    def main(self):
        """
        Wrapper called when the module is loaded, a sample run looks like this;
        ```
        $ python benchmark.py
        2020-05-13 18:38:06.091 | INFO | __main__:main:56 - ** SINGLE Threaded benchmark **
        2020-05-13 18:38:06.092 | INFO | __main__:main:62 - No indexing
        2020-05-13 18:38:06.170 | INFO | __main__:persec:27 - Append: speed=64271/sec | 0:5000
        2020-05-13 18:38:06.249 | INFO | __main__:persec:27 - Append: speed=62946/sec | 5000:5000
        2020-05-13 18:38:06.329 | INFO | __main__:persec:27 - Append: speed=63046/sec | 10000:5000
        2020-05-13 18:38:06.330 | INFO | __main__:main:72 - Indexed by sid, day, hour
        2020-05-13 18:38:06.480 | INFO | __main__:persec:27 - Append: speed=33247/sec | 0:5000
        2020-05-13 18:38:06.628 | INFO | __main__:persec:27 - Append: speed=33954/sec | 5000:5000
        2020-05-13 18:38:06.774 | INFO | __main__:persec:27 - Append: speed=34316/sec | 10000:5000
        2020-05-13 18:38:06.776 | INFO | __main__:main:85 - Compound index
        2020-05-13 18:38:06.894 | INFO | __main__:persec:27 - Append: speed=42132/sec | 0:5000
        2020-05-13 18:38:07.011 | INFO | __main__:persec:27 - Append: speed=43141/sec | 5000:5000
        2020-05-13 18:38:07.126 | INFO | __main__:persec:27 - Append: speed=43451/sec | 10000:5000
        2020-05-13 18:38:07.126 | INFO | __main__:main:95 - Linear scan through most recent index
        2020-05-13 18:38:07.197 | INFO | __main__:persec:27 - Read..: speed=212149/sec | 0:15000
        ```
        """
        logger.info('** SINGLE Threaded benchmark **')

        rmtree('.perfdb', ignore_errors=True)
        manager = Manager()
        database = manager.database('db', '.perfdb')
        table = database.table('sessions')
        logger.info('No indexing')
        self.chunk(database, table, 0, 5000)
        self.chunk(database, table, 5000, 5000)
        self.chunk(database, table, 10000, 5000)
        database.close()

        rmtree('.perfdb')
        manager = Manager()
        database = manager.database('db', '.perfdb')
        table = database.table('sessions')
        logger.info("Indexed by sid, day, hour")
        table.ensure('by_sid', '{sid}')
        table.ensure('by_day', '{day}', duplicates=True)
        table.ensure('by_hour', '{hour}', duplicates=True)
        self.chunk(database, table, 0, 5000)
        self.chunk(database, table, 5000, 5000)
        self.chunk(database, table, 10000, 5000)
        database.close()

        rmtree('.perfdb')
        manager = Manager()
        database = manager.database('db', '.perfdb')
        table = database.table('sessions')
        logger.info("Compound index")
        table.ensure('by_multiple', '!{origin}|{day:02}|{hour:02}|{sid:05}')
        self.chunk(database, table, 0, 5000)
        self.chunk(database, table, 5000, 5000)
        self.chunk(database, table, 10000, 5000)
        database.close()

        manager = Manager()
        database = manager.database('db', '.perfdb')
        table = database.table('sessions')
        logger.info("Linear scan through most recent index")
        start = 0
        count = 15000
        beg = datetime.now()
        for doc in table.find('by_multiple'):
            pass
        self.persec('Read..', start, count, beg)

        database.close()
        rmtree('.perfdb')


if __name__ == '__main__':
    Benchmark().main()

# pynndb2-examples

Example code associated with pynndb version 2.x

#### bulk-import

* example1.py - this is an example routine for bulk-importing data, typically from CSV-like format although any input format can be accomodated. The process is managed by a server process that reads the data from the file, then provides a fan-out to a number of worker process which can manipulate the incoming data, and store it in a database table. Note that this method assumed no indexes and no compression, these needed to be added as a second stage if necessary. On my workstation I'm able to process and store around 245,000 rows per second using example1.py